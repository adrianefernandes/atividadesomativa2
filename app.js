const express = require('express');
const app = express();
const port = process.env.PORT || 3000; 

app.use((req, res, next) => {
  console.log(`Recebida requisição ${req.method} em ${req.url}`);
  next();
});

app.get('/', (req, res) => {
  res.send('Hello, world!');
});


app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
